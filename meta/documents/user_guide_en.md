PANDA.BLACK is the direct route from plentymarkets to Chinese online marketplaces like JD.COM, Kaola, Tmall,
Suning, WeChat etc.

### PANDA.BLACK works in two ways
* NO-RISK: At the moment, PANDA.BLACK lists and sells the articles in China which you have transfered from plentymarkets
  to PANDA.BLACK on our own calculation (pricing, translation, shipping to China, customer support, returns etc.
  are undertaken by PANDA.BLACK).
* CHINA DIRECT: In Q3 2019 follows the version that allows you as seller to sell in your own name via PANDA.BLACK on the
  Chinese marketplaces.

### Set up PANDA.BLACK in plentymarkets
After registering for PANDA.BLACK, you can download the PANDA.BLACK plugin from plentymarkets Marketplace and install it
using the Plugins and Plugin overview menu.

PANDA.BLACK will come up to you after registration to clarify the contractual basis.

After being unlocked by PANDA.BLACK you can match your categories and hand over articles to PANDA.BLACK.

Orders from China are handed over directly to plentymarkets. The delivery address is always an address in Germany.
All other formalities and services are provided by PANDA.BLACK.

### Requirements for using PANDA.BLACK
* Registration at PANDA.BLACK <a href="https://pb.i-ways-network.org/register" target="_blank">via following linkk</a>
* Installation PANDA.BLACK plugin
* Signing contract
* Membership of Bundesverband Onlinehandel e.V. (BVOH)

---  

## USER'S GUIDE PANDA.BLACK

### Integration of the plentymarkets plugin

Using the PANDA.BLACK plugin in plentymarkets requires the following steps:

1. Registration on PANDA.BLACK

	<div class="alert alert-warning">
        To sell products through PANDA.BLACK, you need a PANDA.BLACK account.
        <a href="https://pb.i-ways-network.org/register" target="_blank">Jetzt registrieren</a>
    </div>
    
    ![Description 1](https://cdnmp.plentymarkets.com/9470/meta/images/description_1.png?raw=true)

2. After completing the registration, you will be unlocked by a PANDA.BLACK employee and can now use the plentymarkets
    module.

3. Please log in to your plentymarkets account.

4. After you bought the plugin, install it how it is describe above in the "First steps and requirements".
  
5.  Click on the menu item "System".

    ![Description 2](https://cdnmp.plentymarkets.com/9470/meta/images/description_2.png?raw=true)
    
6.  Click on the menu item "Markets"

    ![Description 3](https://cdnmp.plentymarkets.com/9470/meta/images/description_3.png?raw=true)
    
7.  Click on the sub-item "PANDA.BLACK"

    ![Description 4](https://cdnmp.plentymarkets.com/9470/meta/images/description_4.png?raw=true)

8.  Click on „Authentication“

    ![Description 5](https://cdnmp.plentymarkets.com/9470/meta/images/description_5.png?raw=true)

9.  Now click on the "Authenticate" button. After clicking a login page will open where you enter the login data created
    in step # 1.

    ![Description 6](https://cdnmp.plentymarkets.com/9470/meta/images/description_6.png?raw=true)
    
10. After successful login, please click on "Authorize". Hereby you confirm that products of
    plentymarkets may be handed over to PANDA.BLACK.
    
11. In order to transfer products to PANDA.BLACK, you first need to assign your categories to the existing PANDA.BLACK
    categories. To do this, select on the left side the PANDA.BLACK category in which you have products want to sell.
    On the right side, you select your own category, where the product is currently located.

    ![Description 7](https://cdnmp.plentymarkets.com/9470/meta/images/description_7.png?raw=true)
    
12. Now click on "Create PANDA.BLACK mapping" to complete the assignment of this category.

    ![Description 8](https://cdnmp.plentymarkets.com/9470/meta/images/description_8.png?raw=true)
    
13. After the successful allocation of the categories, you can now go to the Products section to select products to
    transfer to PANDA.BLACK.
    
14. In the "Articles" section, you can now select the products that are in an assigned category to create them on
    PANDA.BLACK. To do this, click on "Create Variation".

    ![Description 9](https://cdnmp.plentymarkets.com/9470/meta/images/description_9.png?raw=true)

    ![Description 10](https://cdnmp.plentymarkets.com/9470/meta/images/description_10.png?raw=true)

15. After creating the variation for PANDA.BLACK, you will have to add the appropiate availability. Please choose "PANDA.BLACK" inside the availability dropdown.
    
    ![Description 11](https://cdnmp.plentymarkets.com/9470/meta/images/description_11.png?raw=true)
    
16. After completing the attribut- and categorymapping, you are able to transfer the products to PANDA.BLACK. By clicking on "Send products to PANDA.BLACK" you can check the status of your items. If we notice any issues with the mapping, you will be notified immediately.

    ![Description 12](https://cdnmp.plentymarkets.com/9470/meta/images/description_12.png?raw=true)     
    
17. The created variations are then processed by plentymarkets and sent to PANDA.BLACK. When you sell a product, you
    will receive a corresponding order confirmation in your order overview.